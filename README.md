# Dark VSCode

Customizable VSCode-like dark color scheme for Qt Creator.

- Linux: `~/.config/QtProject/qtcreator/styles`
- Windows: `C:\Users\Master\AppData\Roaming\QtProject\qtcreator\styles`
